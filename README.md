Materials
=========
Bunch of usefull links.


Hints
-----

* `CheatSheet4Format.ipynb` - Python3 string formating hints for `str.format()` method.
* `CheatSheet4Jupyter.ipynb` - Jupyter Notebook cool features.


**Read more on [WIKI](https://gitlab.com/hell03end/materials/wikis/)**
